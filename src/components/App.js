import React from 'react';
import Footer from './Footer'
import AddToDo from '../containers/AddToDo'
import VisibleTodoList from '../containers/VisibleTodoList'
function App() {
  return (
    <div>
      <AddToDo />
      <VisibleTodoList />
      <Footer />
    </div>
  );
}

export default App;
