import React, { Component } from 'react';
import FilterLink from '../containers/FilterLink'
import { VisibilityFilters } from '../actions'

class Footer extends Component {
  render() {
    return (
      <p>
        Show:
        <FilterLink filter={VisibilityFilters.SHOW_ALL}>All</FilterLink>
        <br/>
        <FilterLink filter={VisibilityFilters.SHOW_ACTIVE}>Active</FilterLink>
        <br/>
        <FilterLink filter={VisibilityFilters.SHOW_COMPLETED}>Completed</FilterLink>
        <br/>
      </p>
    );
  }
}

export default Footer;