import { VisibilityFilters, SET_VISIBILITY_FILTER, ADD_TODO, TOGGLE_TODO } from '../actions';
import { combineReducers } from 'redux';

const todoListInitialState = {
  visibilityFilter: VisibilityFilters.SHOW_ALL,
  todos: []
};

const todos = (state = [], action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [
          ...state.todos,
          {
            text: action.text,
            completed: false
          }
        ]
      }
    case TOGGLE_TODO:
      return {
        ...state,
        todos: state.todos.map((todo, index) => {
          if (index === action.index) {
            return { ...todo, completed: !todo.completed }
          }
          return todo
        })
      }
    default:
      return state
  }
}

const visibilityFilterInitialState = VisibilityFilters.SHOW_ALL;
const visibilityFilter = (state = visibilityFilterInitialState, action) => {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return state.filter
    default:
      return state
  }
}

const todoApp = combineReducers({
  visibilityFilter,
  todos
})
export default todoApp;